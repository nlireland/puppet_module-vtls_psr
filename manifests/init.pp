class vtls_psr (
  $git_repo           = $vtls_psr::params::git_repo,
  $git_branch         = $vtls_psr::params::git_branch,
  $code_path          = $vtls_psr::params::code_path,
  $header_file        = $vtls_psr::params::header_file,
  $footer_file        = $vtls_psr::params::footer_file,
  $output_dir         = $vtls_psr::params::output_dir,
  ) inherits vtls_psr::params {

  include git

  file { $code_path:
    ensure  => 'directory',
    recurse => true,
    mode    => "0664",
    owner   => 'puppetadmin',
    group   => 'puppetadmin',
  }

  if $git_repo == undef {
    fail('The git repo must be defined with the variable: git_repo')
  } else {
    vcsrepo { "$code_path":
      ensure   => latest,
      provider => git,
      source   => $git_repo,
      revision => $git_branch,
      owner    => 'puppetadmin',
      group    => 'puppetadmin',
      user     => 'puppetadmin',
      require  => [Package["git"], File["$code_path"]],
    }
  }

  file { "$code_path/PatronRegister.cgi":
    ensure  => 'present',
    mode    => "0775",
    require => Vcsrepo["$code_path"],
  }

  augeas { "frm-attributes":
    lens => 'Xml.lns',
    incl => "$code_path/conf/vtls.frm",
    changes   => [
      "set FORM/MAIN/#attribute/HEADER $code_path/html/$header_file",
      "set FORM/MAIN/#attribute/FOOTER $code_path/html/$footer_file",
      "set FORM/MAIN/#attribute/OUTPUT_DIR $output_dir",
    ],
    require => Vcsrepo["$code_path"],
  }

  file{"$code_path/lib/conf.pl":
    ensure    => present,
    owner    => 'puppetadmin',
    group    => 'puppetadmin',
    path      => "$code_path/lib/conf.pl",
    content   => template('vtls_psr/conf.pl.erb'),
    replace   => true,
  }

  package { 'perl-XML-Parser':
    ensure  => 'installed',
  }

  include cpan
  cpan { "LWP::UserAgent":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "HTTP::Cookies":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "CGI::Cookie":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "CGI":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "MARC::Record":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "MARC::Field":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "IO":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }

  cpan { "IO::File":
    ensure  => present,
    require => Class['::cpan'],
    force   => true,
  }
}
