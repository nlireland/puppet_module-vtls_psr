class vtls_psr::params{
  $git_repo           = undef
  $git_branch         = "master"
  $code_path          = "/usr/local/vtls_psr"
  $header_file        = "nli-header.inc"
  $footer_file        = "nli-footer.inc"
  $output_dir         = "/usr/local/vtls_psr/output"
}
