# README #

This module installs the VTLS PSR application from a remote git repo

## Set-up for VTLS PSR Instance##

Example node definition with Apache config etc:

```
node 'mynode' {
  $doc_home = "/usr/local/vtls_psr"

  netrc::foruser{"netrc_user":
    user => 'puppetadmin',
    machine_user_password_triples => [
      ['bitbucket.org', hiera('bitbucket_login'), hiera('bitbucket_password')]
    ],
  }

  class { 'vtls_psr':
    vufind_git_repo     => 'https://bitbucket.org/USER_ACCOUNT/application_name.git',
    code_path           => $doc_home,
    require             => Netrc::Foruser["netrc_user"],
  }

  class { 'apache':
    default_mods        => false,
    default_confd_files => false,
    mpm_module          => 'prefork',
    require             => Class['vtls_psr'],
  }

  $vhost_name = "example.com"
  apache::vhost{ $vhost_name:
    default_vhost  => true,
    manage_docroot => false,
    docroot        => "$doc_home",
    directories    => {},
    servername     => $vhost_name,
  }
}
```

